import json
from os import PathLike

def mapper(extracted_file,data_model_file):
    try:
        with open(extracted_file) as f:
            ex_data = json.load(f)
            for mapping in ex_data['mappings']:
                sources = mapping['source']
                target = mapping['target']
                target = []
                for source in sources:
                    with open(data_model_file) as f:
                        data = json.load(f)
                        try:
                            siem_field = data[source['field']]
                            if source['data_model'] in siem_field:
                                data_model = source['data_model']
                            else:
                                data_model = "default"
                            siem_field_data_model = siem_field[data_model]
                            src_data_type = siem_field_data_model['data_type']
                            src_description = siem_field_data_model['description']
                            source.update({"type":src_data_type,"description":src_description,})
                            el = siem_field_data_model['elastic']
                            if el["field"] != "":
                                el_obj = {
                                'field':el['field'],
                                'description':el['description'],
                                'type':el['data_type'],
                                'data_model':""
                            }
                            else:
                                el_obj= ""
                            target.append(el_obj)
                        except Exception:
                            target.append("")
                mapping.update({'target':target})
        with open("new_squid_proxy_extraction.json","w") as f:
            json.dump(ex_data,f,indent=2)
        return ex_data
    except IOError as e:
        print(e)
    except Exception as e:
        print("Enter valid files")

    

if __name__ == "__main__":
    try:
        ex_data = mapper("squid_proxy_extraction.json","static_mapping_final.json")
        # print(ex_data)
    except TypeError as e:
        print(e)
    



